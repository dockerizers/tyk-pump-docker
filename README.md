# Tyk Pump Docker Image with Telegraf and Git setup

The Repo has two branches. `master` and `release`. Once a release branch is updated, the docker image should be built and pushed to the gitlab docker registry. 


Image: registry.gitlab.com/dockerizers/tyk-pump-docker

```yaml
environment:
    - TYK_PMP_CONF=/opt/tyk-pump/pump.conf

volume:
    - ./config/pump.conf:/opt/tyk-pump/pump.conf
    - ./config/telegraf/pump.conf:/etc/telegraf/telegraf.conf:ro

command:
    - bash
    - -c
    - |
    # telegraf --config telegraf.conf &
    service telegraf start # --config /etc/telegraf/telegraf.conf
    /opt/tyk-pump/tyk-pump -c $${TYK_PMP_CONF}
```

# Image Versioning
When pushing a new image, ensure that teh VERSION variable in `.gitlab-ci` matches the docker image version.