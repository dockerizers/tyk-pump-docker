FROM tykio/tyk-pump-docker-pub:v1.6.0
LABEL maintainer="Kariuki Gathitu <kgathi2@gmail.com>"
LABEL version="1.6"

ENV DISTRO_NAME=buster

# Before adding telegraf Influx repository, run this so that apt will be able to read the repository.
RUN apt-get update && apt-get install -y apt-transport-https wget gnupg
# Add the telegraf InfluxData key
RUN wget -qO- https://repos.influxdata.com/influxdb.key | apt-key add -
RUN echo "deb https://repos.influxdata.com/debian ${DISTRO_NAME} stable" | tee /etc/apt/sources.list.d/influxdb.list

ENV CORE_PACKAGES="telegraf"

RUN apt-get update && apt-get install -y $CORE_PACKAGES && apt-get clean
